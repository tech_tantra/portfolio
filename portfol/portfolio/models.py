from django.db import models

# Create your models here.
class Feedback(models.Model):
    fname=models.CharField(max_length=25)
    lname=models.CharField(max_length=25)
    email=models.EmailField()
    country = models.CharField(max_length=20, null=True)
    comments=models.TextField()
    timestamp=models.DateTimeField(auto_now_add=True, blank=True)
    
    def __str__(self):
        return(self.fname)