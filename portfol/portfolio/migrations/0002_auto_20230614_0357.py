# Generated by Django 3.2.5 on 2023-06-14 03:57

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('portfolio', '0001_initial'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Contact',
            new_name='Feedback',
        ),
        migrations.RenameField(
            model_name='feedback',
            old_name='description',
            new_name='comments',
        ),
    ]
