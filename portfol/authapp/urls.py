from django.urls import path
from authapp import views

urlpatterns = [
    path('signup/', views.signupmodule, name='signupmodule'),
    path('signin/', views.signinmodule, name='signinmodule'), 
    path('logout/', views.logoutmodule, name='logoutmodule'),
    path('feedback/', views.feedbackmodule, name='feedbackmodule'),
]
