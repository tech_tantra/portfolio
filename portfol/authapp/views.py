from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages

# Create your views here.
def signinmodule(request):
    if request.method =="POST":
        get_email= request.POST.get('email')
        get_pass = request.POST.get('pass1')
        myuser= authenticate(username=get_email, password=get_pass)
        
        if myuser is not None:
            login(request, myuser)
            messages.success(request, "Login is success")
            return redirect ('/')
        else:
            messages.error(request, "Invalid Email or Password")
    
    
    return render(request, 'signin.html')

def signupmodule(request):
    if request.method =="POST":
        get_email= request.POST.get('email')
        get_pass = request.POST.get('pass1')
        get_confirm_pass = request.POST.get('pass2')

        if get_pass != get_confirm_pass:
            messages.info(request, 'Password not matched!!')
            return redirect('/auth/signup/')
        try:
            if User.objects.get(username=get_email):
                messages.warning(request, "Email is already taken")
                return redirect('/auth/signup/')
        except Exception as identifier:
            pass
        myuser = User.objects.create_user(get_email, get_email, get_pass)
        myuser.save()
        messages.success(request, 'New user is Created, please login')
        return redirect('/auth/signin/')
        
    
    return render(request, 'signup.html')

def logoutmodule(request):
    logout(request)
    messages.success(request, 'Logged out successfully!!') 
    return render(request, 'signin.html')

def feedbackmodule(request):
    return render(request, 'feedback.html')